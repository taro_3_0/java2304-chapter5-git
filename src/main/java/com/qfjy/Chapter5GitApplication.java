package com.qfjy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Chapter5GitApplication {

    public static void main(String[] args) {
        SpringApplication.run(Chapter5GitApplication.class, args);
    }

}
